"""M345SC Homework 1, part 2
Your name and CID here
Sangwoo Jo  CID : 01207818
"""

import numpy as np
import time
import matplotlib.pyplot as plt

#Search for a certain target number in a list using the binary search method
def bsearch(L,target):
    """Input:
    L: List of numbers in an ascending order
    target: The number to be searched in L

    Output:
    imid: The index of which target is found in L. If the target is not found
    return -1
    """
    istart = 0
    iend = len(L)-1

    #Contract "active" portion of list
    while istart<=iend:
        imid = int(0.5*(istart+iend))
        if target == L[imid]:
            return imid
        elif target < L[imid]:
            iend = imid-1
        else:
            istart = imid+1
    return -1

def nsearch(L,P,target):
    """Input:
    L: list containing *N* sub-lists of length M. Each sub-list
    contains M numbers (floats or ints), and the first P elements
    of each sub-list can be assumed to have been sorted in
    ascending order (assume that P<M). L[i][:p] contains the sorted elements in
    the i+1th sub-list of L
    P: The first P elements in each sub-list of L are assumed
    to be sorted in ascending order
    target: the number to be searched for in L

    Output:
    Lout: A list consisting of Q 2-element sub-lists where Q is the number of
    times target occurs in L. Each sub-list should contain 1) the index of
    the sublist of L where target was found and 2) the index within the sublist
    where the target was found. So, Lout = [[0,5],[0,6],[1,3]] indicates
    that the target can be found at L[0][5],L[0][6],L[1][3]. If target
    is not found in L, simply return an empty list (as in the code below)
    """

    Lout=[] #Output of the function
    N=len(L)
    M=len(L[0])

    for i in range(N):
        #For the sorted section of L[i]
        ind = bsearch(L[i][:P],target)
        #Store the index where the target is found in L[i][:P]


        #Note that the bsearch method only find one index.
        #Hence, when the target is found, search for other indices where
        #the same target is found.

        #Use the fact that L[i][:P] is sorted in an asceding order
        if ind != -1:
            for j in range(1,ind+1):
                if L[i][ind-j] == target :
                    Lout.append([i,ind-j])
                else:
                    break
            Lout.append([i,ind])

            for j in range(1,P-ind):
                if L[i][ind+j] == target:
                    Lout.append([i,ind+j])
                else:
                    break

        #For the non-sorted section of L[i]
        #Linear search as binary search cannot be used. I tempted to sort
        #the list first and apply the binary search method after.
        #However, the method was not efficient in terms of running time.
        for j,k in enumerate(L[i][P:]):
            if k == target:
                Lout.append([i,j+P])
    return Lout

def nsearch2(L,P,target):
    """Same input and output as nsearch function
    The function simply uses the linear search method for all L[i]'s. nsearch2
    function is designed to make a vivid comparison between the linear and
    the binary search algorithm.
    """
    Lout=[]
    N=len(L)
    M=len(L[0])

    for i in range(N):
        for j,k in enumerate(L[i]):
            if k == target :
                Lout.append([i,j])
    return Lout

def nsearch_time(N,M,method):
    """Analyze the running time of nsearch.
    Add input/output as needed, add a call to this function below to generate
    the figures you are submitting with your codes.

    Input:
    N: The number of lists within L
    M: The length of each list that L contains
    method: The value is 1,2,or 3. If the method is 1, M will be the changing
    variable while N remains constant. If the method is 2, N will be the
    changing variable and M stays the same. If the method is 3, the graph will
    explicitly show the trends in running time for each function.

    Output:
    Plot that displays two graphs. Each graph represents the running time when
    running either the nsearch or the nsearch2 function

    Discussion: (add your discussion here)
    As explained previously, nsearch2 function simply uses the linear search
    for all lists within L. On the other hand, nsearch fucntion combines both
    linear and binary search. It will compute a binary search for the first P
    elements that are sorted within L[i]'s.
    Before jumping into the plot, let's analyze the runnning time for both
    algorithms. For the nsearch2 function, as the cost of the linear search
    method is O(n) where n is the number of elements in a list, the running
    time will be O(NM). To be more specific, there are 3 assignments outside
    of the loop, and 1 comparison with 3 assignments inside the loop that has
    NM iterations. Hence, the running time will be 3+4*N*M in the worst case
    and 3+2*N*M in average.
    On the other hand, for the nsearch function, as I computed a binary search
    method for the first P elements for each of the list in L, the cost
    function of that particular section will be O(log(2)P). To summarize, first
    of all, there are 3 initial assignments. Once the binary search method is
    computed, I check if there are any other indices within L[i][:P] that
    contain the same target. For this, there are 1 comparison and
    1 assignment maximum for each index that have the same target. Hence, the
    cost function for this case will be 2*X maximum where X will be the number
    of indices that contain the target within the first P elements. Finally,
    running time for the linear search method for rest of the M-P elements for
    each list will be O(M-P). In total, the running time is around
    3+N(4*(M-P)+log(2)P+2*X+1).
    Therefore, I can conclude that the nsearch function is more an efficient
    algorithm than nsearch2 when the values of N and M increase. The gap will
    especially increase when the value of P gets larger.

    As shown in fig1 or fig2, the gap of the two running time increases as
    either N or M increases. Note how fig1 depicts how the cost for nsearch
    function is actually higher when the value of M is small. For fig2, this
    is not the case as we fixed the value of M as 200, which may set P
    already high enough so that the running time for the nsearch function is
    faster than nsearch2.

    fig3 shows the order of the running time for each algorithm. Each of the
    two graph seems to converge to a certain value as the value of M increases.
    Hence, this plot confirms the order of the cost function I computed in the
    previous paragraph. The trend is obvious for the nsearch2 function.
    However, note that the blue line is not completely converging to a
    constant, as there is a 2*x factor in the cost function where x is the
    number of indices that contain the target value within the first P elements
    of each list.
    """
    if method == 1: #Observe the change of running time given N
        target = 50 #Set the target as 50 for convenience
        x = np.linspace(1,M,M)
        nsearch_t = np.zeros(M)
        nsearch2_t = np.zeros(M)

        for i in range(1,M+1): #The length of L[0] ranges from 1 to M
            L = []
            for j in range(N):
                L.append([np.random.randint(1,100) for r in range(i)])

            P = i//2 #Set P as half of M for convenience

            #Sort the first P elements for each list
            for j in range(N):
                L[j][:P] = np.sort(L[j][:P],kind = 'mergesort')

            t1 = time.time()
            nsearch(L,P,target)
            t2 = time.time()
            nsearch_t[i-1] = t2-t1

            t3 = time.time()
            nsearch2(L,P,target)
            t4 = time.time()
            nsearch2_t[i-1] = t4-t3

        plt.plot(x,nsearch_t,label="Running time for nsearch")
        plt.plot(x,nsearch2_t,label="Running time for nsearch2")
        plt.xlabel("The value of M given the value of N=%f"%(N))
        plt.ylabel("Running Time")
        plt.title("Comparison for the two methods \n Sangwoo Jo nsearch_time(200,200,1)")
        plt.legend()
        plt.show()

    if method == 2: #Observe the change of running time given M
        target = 50
        x = np.linspace(1,N,N)
        nsearch_t = np.zeros(N)
        nsearch2_t = np.zeros(N)
        L = []

        for i in range(1,N+1):
            L.append([np.random.randint(1,100) for r in range(M)])

            P = M//2 #Set P as half of M for convenience

            #Sort the first P elements for each list
            for j in range(i):
                L[j][:P] = np.sort(L[j][:P],kind = 'mergesort')

            t1 = time.time()
            nsearch(L,P,target)
            t2 = time.time()
            nsearch_t[i-1] = t2-t1

            t3 = time.time()
            nsearch2(L,P,target)
            t4 = time.time()
            nsearch2_t[i-1] = t4-t3

        plt.plot(x,nsearch_t,label="Running time for nsearch")
        plt.plot(x,nsearch2_t,label="Running time for nsearch2")
        plt.xlabel("The value of N given the value of M=%f"%(M))
        plt.ylabel("Running Time")
        plt.title("Comparison for the two methods \n Sangwoo Jo nsearch_time(200,200,2)")
        plt.legend()
        plt.show()

    if method == 3: #N is fixed. Observe the trend in running times
        target = 50 #Set the target as 50 for convenience
        x = np.linspace(1,M,M)
        nsearch_tr = np.zeros(M)
        nsearch2_tr = np.zeros(M)

        for i in range(1,M+1): #The length of L[0] ranges from 1 to M
            L = []
            for j in range(N):
                L.append([np.random.randint(1,100) for r in range(i)])

            P = i//2 #Set P as half of M for convenience

            #Sort the first P elements for each list
            for j in range(N):
                L[j][:P] = np.sort(L[j][:P],kind = 'mergesort')

            t1 = time.time()
            nsearch(L,P,target)
            t2 = time.time()
            #Divide by the order
            nsearch_tr[i-1] = (t2-t1)/(N*(1+int(np.log2(P+1))+4*(i-P)))
            t3 = time.time()
            nsearch2(L,P,target)
            t4 = time.time()
            #Divide by the order
            nsearch2_tr[i-1] = (t4-t3)/(N*i)

        plt.plot(x,nsearch_tr,label="Running time for nsearch/(N*(1+log(2)P+4(M-P)))")
        plt.plot(x,nsearch2_tr,label="Running time for nsearch2/N*M")
        plt.xlabel("The value of M given the value of N=%f"%(N))
        plt.ylabel("Running Time")
        plt.title("Comparison for the two methods \n Sangwoo Jo nsearch_time(400,400,3)")
        plt.legend()
        plt.show()



if __name__ == '__main__':

    #add call(s) to nsearch here which generate the figure(s)
    #you are submitting
    nsearch_time(200,200,1) #modify as needed
    nsearch_time(200,200,2)
    nsearch_time(400,400,3)
