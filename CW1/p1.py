"""M345SC Homework 1, part 1
Your name and CID here
Sangwoo Jo  CID : 01207818
"""

import numpy as np
import time

def ksearch(S,k,f,x):
    """
    Search for frequently-occurring k-mers within a DNA sequence
    and find the number of point-x mutations for each frequently
    occurring k-mer.
    Input:
    S: A string consisting of A,C,G, and Ts
    k: The size of k-mer to search for (an integer)
    f: frequency parameter -- the search should identify k-mers which
    occur at least f times in S
    x: the location within each frequently-occurring k-mer where
    point-x mutations will differ.

    Output:
    L1: list containing the strings corresponding to the frequently-occurring k-mers
    L2: list containing the locations of the frequent k-mers
    L3: list containing the number of point-x mutations for each k-mer in L1.

    Discussion: Add analysis here
    For constructing L1, I first made a dictionary that contains the number of
    appearance for each k-mer's. Then, I simply extracted the keys that have
    the value greater or equal to f.
    When computing L2, for each k-mer I used a linear search to see the exact
    locations the according k-mer was found.
    Finally, when constructing L3, for each k-mer, if the x-th element is
    different, I will compare the rest of the elements if they are identical.
    If so, I will add 1 to the appropriate element in the list L3.

    Now, let's analyze the running time when computing each list L1,L2, and L3.
    To start with, the method used to compute L1 is by constructing a
    dictionary and simply output the keys that have a value greater or equal
    to f. Hence, the running time will be in order of nS-k, where nS is the
    length of the input sequence S. To be precise, since there are
    2 assignments and 1 comparison for nS-k loops, the cost of designing a
    dictionary is 3*(nS-k). Furthermore, the cost of outputting the keys that
    have a value greater or equal to f is 2*(nS-k) as there are 1 comparison
    and 1 assignment maximum for nS-k loops. Therefore, the toal running time
    computing L1 will be 5*(nS-k) in the worst case. Hence, the leading order
    term will be (nS-k). In other words, the cost is O(nS-k).

    L2 is first computed by assigning it as a list that contains "len(L1)"
    empty lists. Then, inside the loop of nS-k iterations, there are
    1 assignment and an another loop of len(L1) iterations that have
    2 operations maximum. Hence, the total running time for computing L2 is
    (nS-k)*(1+2*len(L1))+len(L1) in the worst case and
    (nS-k)*(1+len(L1))+len(L1) in average. Hence, likewise, the leading order
    term is (nS-k). In other words, the cost will be asymptotically O(nS-k).

    Finally, let's compute the running time for computing L3. First of all,
    there is 1 assignment of L3. Then, for len(L1)*(nS-k) iterations,
    there are 1 assignment of R and an "if" loop that contains 3 operations
    maximum. Hence, the cost of computing L3 will be approximately
    4*(nS-k)*len(L1) + 1. Likewise, the leading order term is (nS-k). In other
    words, the cost will be asymptotically O(nS-k).

    As a conclusion, we can see that the running time for computing these
    three lists mostly rely on the length of the input sequence S and k.

    """

    nS = len(S)
    L1,L2,L3=[],[],[]

    D ={}
    #Construct a dictionary
    #key: k-mer's
    #value: number of appearance in the input sequence S for each k-mer

    t1 = time.time()
    for i in range(nS-k+1):
        P = S[i:i+k]
        if P in D:
            D[P] = D[P]+1
        else:
            D[P]=1

    #Append on L1 for the k-mer's that have a value greater or equal to f
    for d in D:
        if D[d]>=f:
            L1.append(d)

    t2 = time.time()
    print("time computed for L1=",t2-t1)

    #Construct L2 as a list that contains "len(L1)" empty lists
    for i in range(len(L1)):
        L2.append([])

    for i in range(nS-k+1):
        Q = S[i:i+k]
        for j in range(len(L1)):
            if Q == L1[j]:
                L2[j].append(i)

    t3 = time.time()
    print("time computed for L2=",t3-t2)

    L3 = np.zeros(len(L1))

    for i in range(len(L1)):
        for j in range(nS-k+1):
            R=S[j:j+k]
            if R[x] != L1[i][x] : #Divide into 3 cases
                if x == 0:
                    if R[1:] == L1[i][1:]:
                        L3[i] = L3[i]+1
                elif x == k-1:
                    if R[:k-1] == L1[i][:k-1]:
                        L3[i] = L3[i]+1
                else :
                    if R[:x] == L1[i][:x]:
                        if  R[x+1:] == L1[i][x+1:]:
                            L3[i] = L3[i]+1
    t4 = time.time()
    print("time computed for L3=",t4-t3)

    return L1,L2,L3




if __name__=='__main__':
    #Sample input and function call. Use/modify if/as needed
    S='CCCTATGTTTGTGTGCACGGACACGTGCTAAAGCCAAATATTTTGCTAGT'
    k=3
    x=2
    f=2
    L1,L2,L3=ksearch(S,k,f,x)
    print("L1=",L1)
    print("L2=",L2)
    print("L3=",L3)
