"""M345SC Homework 3, part 1
Your name and CID here
Sangwoo Jo  CID : 01207818
"""
import numpy as np
import scipy as sp
import time
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from scipy.signal import hann

def nwave(alpha,beta,Nx=256,Nt=801,T=200,display=False):
    """
    Question 1.1
    Simulate nonlinear wave model

    Input:
    alpha, beta: complex model parameters
    Nx: Number of grid points in x
    Nt: Number of time steps
    T: Timespan for simulation is [0,T]
    Display: Function creates contour plot of |g| when true

    Output:
    g: Complex Nt x Nx array containing solution
    """

    #generate grid
    L = 100
    x = np.linspace(0,L,Nx+1)
    x = x[:-1]

    n = np.fft.fftshift(np.arange(-Nx/2,Nx/2))
    k = 2*np.pi*n/L

    def RHS(f,t,alpha,beta):
        """Computes dg/dt for model eqn.,
        f[:N] = Real(g), f[N:] = Imag(g)
        Called by odeint below
        """
        g = f[:Nx]+1j*f[Nx:]
        #add code here
        #Compute first derivative
        g_fft = np.fft.fft(g)/Nx
        dg = Nx*np.fft.ifft(1j*k*g_fft)
        #Compute second derivative
        dg_fft = np.fft.fft(dg)/Nx
        d2g = Nx*np.fft.ifft(1j*k*dg_fft)

        #-----------
        dgdt = alpha*d2g + g -beta*g*g*g.conj()
        df = np.zeros(2*Nx)
        df[:Nx] = dgdt.real
        df[Nx:] = dgdt.imag
        return df

    #set initial condition
    g0 = np.random.rand(Nx)*0.1*hann(Nx)
    f0=np.zeros(2*Nx)
    f0[:Nx]=g0
    t = np.linspace(0,T,Nt)

    #compute solution
    f = odeint(RHS,f0,t,args=(alpha,beta))
    g = f[:,:Nx] + 1j*f[:,Nx:]

    if display:
        plt.figure()
        plt.contour(x,t,g.real)
        plt.xlabel('x')
        plt.ylabel('t')
        plt.title('Contours of Real(g)')

    return g

def analyze(Nx=256,Nt=801,T=200):
    """
    Question 1.2
    Add input/output as needed

    Discussion: Add discussion here
    """

    """
    Figure 1 shows the mean amplitude of the coefficients of c_n for case A and
    case B. The distribution seems to be nearly identical besides that the ones
    for case B are slightly smaller on both ends. We can still observe how the
    most energetic wavenumber and frequency are 0 for both cases. The energy
    peaks when n=0 as slowly decay on both sides.

    From Figure 2 to 5, I plotted the distribution of the c_n's over different
    time span. As shown in the figures, all four plot fluctuate in a similar
    matter. Hence, from this point, I will only consider the last row
    of the "g1_fft" matrix.

    Rest of the three figures depict the change of overall distribution of c_n
    for case A by changing Nx,Nt, and T respectively. For all three figures,
    however, the trend seems to be quite the same. Figure 6 shows a
    different plot only because the range of n changed due to change of Nx.
    To add on, there seems to be less fluctation with higher concentration
    towards the maximum when the values of Nx,Nt,and T increases. However, the
    overall distribution is nearly identical. To conclude, for all the plots I
    plotted, the amplitude of the energy peaks when n=0.
    """

    g1 = nwave(1-2j,1+2j)
    g2 = nwave(1-1j,1+2j)

    t_bound = int(50*(Nt-1)/T)

    g1_fft = np.fft.fft(g1[t_bound:,])/Nx
    g2_fft = np.fft.fft(g2[t_bound:,])/Nx

    n = np.arange(-Nx/2,Nx/2)

    #Mean Amplitude
    plt.figure()
    plt.semilogy(n,np.fft.fftshift(np.abs(g1_fft).mean(axis=0)),'x')
    plt.semilogy(n,np.fft.fftshift(np.abs(g2_fft).mean(axis=0)),'r--')

    plt.legend(('Case A','Case B'))
    plt.xlabel('n')
    plt.ylabel('Mean amplitude of Fourier Coefficients : Nx=256')
    plt.title('Mean amplitude of Fourier Coefficients : Nx=256 \n Sangwoo Jo analyze()')

    #Amplitude distribution over different time span
    n0 = np.linspace(0,600,4)

    for i in range(len(n0)):
        plt.figure()
        plt.semilogy(n,np.fft.fftshift(np.abs(g1_fft[int(n0[i]),])),'x')
        plt.xlabel('n')
        plt.ylabel('Amplitude of Fourier Coefficients')
        plt.title('Amplitude of Fourier Coefficients : t=%f \n Sangwoo Jo analyze()'%(n0[i]))

    #Switch Nx
    g12 = nwave(1-2j,1+2j,Nx=100)
    g12_fft = np.fft.fft(g12[50:,])/100
    n2 = np.arange(-100/2,100/2)


    plt.figure()
    plt.semilogy(n2,np.fft.fftshift(np.abs(g12_fft[-1,])),'x')
    plt.semilogy(n,np.fft.fftshift(np.abs(g1_fft[-1,])),'.')
    plt.legend(('Nx=100','Nx=256'))
    plt.xlabel('n')
    plt.ylabel('Amplitude of Fourier Coefficients')
    plt.title('Amplitude of Fourier Coefficients : Nx \n Sangwoo Jo analyze()')

    #Switch Nt
    g13 = nwave(1-2j,1+2j,Nt=200)
    g13_fft = np.fft.fft(g13[50:,])/Nx

    plt.figure()
    plt.semilogy(n,np.fft.fftshift(np.abs(g13_fft[-1,])),'x')
    plt.semilogy(n,np.fft.fftshift(np.abs(g1_fft[-1,])),'.')
    plt.legend(('Nt=200','Nt=801'))
    plt.xlabel('n')
    plt.ylabel('Amplitude of Fourier Coefficients')
    plt.title('Amplitude of Fourier Coefficients : Nt \n Sangwoo Jo analyze()')

    #Switch T
    g14 = nwave(1-2j,1+2j,T=100)
    g14_fft = np.fft.fft(g14[50:,])/Nx

    plt.figure()
    plt.semilogy(n,np.fft.fftshift(np.abs(g14_fft[-1,])),'x')
    plt.semilogy(n,np.fft.fftshift(np.abs(g1_fft[-1,])),'.')
    plt.legend(('T=100','T=200'))
    plt.xlabel('n')
    plt.ylabel('Amplitude of Fourier Coefficients')
    plt.title('Amplitude of Fourier Coefficients : T \n Sangwoo Jo analyze()')

    return None #modify as needed


def wavediff(Nx=256,Nt=801,T=100,display=False):
    """
    Question 1.3
    Add input/output as needed

    Discussion: Add discussion here
    """

    """Observing the first figure, we can observe how both methods show nearly
    identical result in calculating the dg/dx. The coefficients are especially
    close to each other every where besides the endpoints. The finite difference
    method implements the one-sided 4th order scheme when calculating the
    derivatives of the end points, but the method does not seem to be efficient
    as when calculating the non-boundary coefficients. The second plot depicts
    explicitly of the overall trend of the absolute difference between the two
    against the x values. The difference is around 10^(-6) to 10^(-9), while
    the difference is to the scale of 10^(-2) at the ends.

    Furthermore, the "wavediff" outputs the time consumed when running each
    method. The time taken when implementing the finite difference method
    seems to be 5 to 10 times slower than when computing the discrete fourier
    transform method. Hence, we can conclude that the discrete fourier transform
    is a more efficient method when calculating the coefficients for dg/dx.
    """
    g = nwave(1-1j,1+2j,T=100)[99,] #Compute the solution g at t=100
    L = 100

    x = np.linspace(0,L,Nx+1)
    x = x[:-1]

    #DFT Method
    t1 = time.time()
    n = np.fft.fftshift(np.arange(-Nx/2,Nx/2))

    k = 2*np.pi*n/L
    #Compute first derivative
    g_fft = np.fft.fft(g)/Nx
    dg = Nx*np.fft.ifft(1j*k*g_fft)

    t2 = time.time()

    #Finite Difference Method
    t3 = time.time()
    h = L/Nx #Distance between the grids

    #RHS
    a = 25/16
    b = 1/5
    c = -1/80
    #LHS
    ag = 3/8

    #Construct matrix A using scipy.linalg
    zv = np.ones(Nx)
    agv = ag*zv[1:]

    A = sp.sparse.diags([agv,zv,agv],[-1,0,1])
    A = A.toarray()
    y = np.zeros(Nx,dtype=complex)

    y[3:Nx-3]=a*(g[4:Nx-2]-g[2:Nx-4])/(2*h) + b*(g[5:Nx-1]-g[1:Nx-5])/(4*h) + c*(g[6:Nx]-g[0:Nx-6])/(6*h)

    #rest of the y values
    y[1] = a*(g[2]-g[0])/(2*h) + b*(g[3]-g[-1])/(4*h) + c*(g[4]-g[-2])/(6*h)
    y[2] = a*(g[3]-g[1])/(2*h) + b*(g[4]-g[0])/(4*h) + c*(g[5]-g[-1])/(6*h)
    y[Nx-3] = a*(g[Nx-2]-g[Nx-4])/(2*h) + b*(g[Nx-1]-g[Nx-5])/(4*h) + c*(g[0]-g[Nx-6])/(6*h)
    y[Nx-2] = a*(g[Nx-1]-g[Nx-3])/(2*h) + b*(g[0]-g[Nx-4])/(4*h) + c*(g[1]-g[Nx-5])/(6*h)


    #Use 4th order "one-sided" scheme
    ag2 = 3
    a2,b2,c2,d2 = -17/6,3/2,3/2,-1/6

    #Use the "solve_banded" method
    Ab = np.zeros((3,Nx))
    Ab[0,1:] = np.diag(A,k=1)
    Ab[1,:] = np.diag(A)
    Ab[2,:Nx-1] = np.diag(A,k=-1)

    dg_fd = sp.linalg.solve_banded((1,1),Ab,y)

    #Assign the first and last element
    dg_fd[0] = (a2*g[0]+b2*g[1]+c2*g[2]+d2*g[3])/h-ag2*dg_fd[1]
    dg_fd[Nx-1] = -(a2*g[Nx-1]+b2*g[Nx-2]+c2*g[Nx-3]+d2*g[Nx-4])/h-ag2*dg_fd[Nx-2]
    t4 = time.time()

    if display == 1:
        plt.figure()
        plt.semilogy(x,np.abs(dg))
        plt.semilogy(x,np.abs(dg_fd))
        plt.legend(('DFT','Finite Difference Method'))
        plt.xlabel('x')
        plt.ylabel('Amplitude of Derivatives')
        plt.title('DFT vs Finite Difference Method \n Sangwoo Jo wavediff(display=1)')

    if display == 2:
        plt.figure()
        plt.semilogy(x,np.abs(np.abs(dg)-np.abs(dg_fd)))
        plt.legend(('Absolute Difference'))
        plt.xlabel('x')
        plt.ylabel('Difference of the Derivatives')
        plt.title('DFT vs Finite Difference Method 2 \n Sangwoo Jo wavediff(display=2)')

    return("Time running DFT:",t2-t1, "Time running Finite Difference Method",t4-t3)


if __name__=='__main__':
    x=None
    #Add code here to call functions above and
    #generate figures you are submitting

    output1=nwave(1-2j,1+2j,display=True)
    output2=nwave(1-1j,1+1j,display=True)
    output3=analyze()
    output4=wavediff(display=1)
    output5=wavediff(display=2)

    print("output1=",output1)
    print("output2=",output2)
    print("output3=",output3)
    print("output4=",output4)
    print("output5=",output5)
