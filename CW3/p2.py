"""M345SC Homework 3, part 2
Your name and CID here
Sangwoo Jo  CID : 01207818
"""
import numpy as np
import networkx as nx
import scipy
from scipy.integrate import odeint

def growth1(G,params=(0.02,6,1,0.1,0.1),T=6):
    """
    Question 2.1
    Find maximum possible growth, G=e(t=T)/e(t=0) and corresponding initial
    condition.

    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    T: time for energy growth

    Output:
    G: Maximum growth
    y: 3N-element array containing computed initial condition where N is the
    number of nodes in G and y[:N], y[N:2*N], y[2*N:] correspond to S,I,V

    Discussion: Add discussion here
    """

    """
    I interpreted the question as a matrix operator question, where given A we
    want to find x so that the length of b is maximized with length of x being 1.
    If we express the question into dx/dt = Mx form, we can solve the ODE as the
    following: x(t) = A(t)*x(0). Hence, this goes back to the matrix operator
    question I just discussed.

    Computing the eigenvalues and eigenvectors of A^T*A as discussed in lectures,
    I get a maximum growth of e(t=T)/e(t=0) as 270.
    """

    a,theta,g,k,tau=params
    N = G.number_of_nodes()

    #Construct flux matrix (use/modify as needed)
    Q = [t[1] for t in G.degree()]

    Pden = np.zeros(N)
    Pden_total = np.zeros(N)
    for j in G.nodes():
        for m in G.adj[j].keys():
            Pden[j] += Q[m]
    Pden = 1/Pden
    Q = tau*np.array(Q)
    F = nx.adjacency_matrix(G).toarray()
    F = F*np.outer(Q,Pden)
    #-------------------------------------
    G=0
    y = np.zeros(3*N)
    #Add code here

    #Define M
    M  = np.zeros((3*N,3*N))
    M[:N,:N] = F-np.identity(N)*(g+k+tau)
    M[:N,N:2*N] = np.identity(N)*a
    M[N:2*N,:N] = np.identity(N)*theta
    M[N:2*N,N:2*N] = F-np.identity(N)*(k+a+tau)
    M[2*N:3*N,:N] = np.identity(N)*(-theta)
    M[2*N:3*N,2*N:3*N] = F+np.identity(N)*(k-tau)

    A = scipy.linalg.expm(M*T)

    #Compute eigenvalue of A^T*A
    l,v = np.linalg.eig(np.dot(A.T,A))

    x0 = v[:,np.argmax(l)] #Eigenvector corresponding to the maximum eigenvalue

    #When the length of b is maximized, the length is the maximum eigenvalue
    return ("max_growth = ",np.abs(max(l))/np.linalg.norm(x0)**2,
    "inital condition = ",x0)



def growth2(G,params=(0.02,6,1,0.1,0.1),T=6):
    """
    Question 2.2
    Find maximum possible growth, G=sum(Ii^2)/e(t=0) and corresponding initial
    condition.

    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    T: time for energy growth

    Output:
    G: Maximum growth
    y: 3N-element array containing computed initial condition where N is the
    number of nodes in G and y[:N], y[N:2*N], y[2*N:] correspond to S,I,V

    Discussion: Add discussion here
    """

    """
    I used the same implementation method for this question. The only different
    part is that I extracted the middle N rows from A, as we only care about
    the I_i's. If we define the extracted N*3N matrix as A', then we are solving
    the same equation: x(t)=A'(t)*x(0) where x(0) only contains the I_i's this
    time.

    Once we compute the maximum eigenvalue and the corresponding eigenvector, we
    obtain the maximum growth of sum(I_i(t=T)^2)/e(t=0) as 36.
    """

    a,theta,g,k,tau=params
    N = G.number_of_nodes()

    #Construct flux matrix (use/modify as needed)
    Q = [t[1] for t in G.degree()]

    Pden = np.zeros(N)
    Pden_total = np.zeros(N)
    for j in G.nodes():
        for m in G.adj[j].keys():
            Pden[j] += Q[m]
    Pden = 1/Pden
    Q = tau*np.array(Q)
    F = nx.adjacency_matrix(G).toarray()
    F = F*np.outer(Q,Pden)
    #-------------------------------------
    G=0
    y = np.zeros(3*N)

    #Add code here

    #Define M
    M  = np.zeros((3*N,3*N))
    M[:N,:N] = F-np.identity(N)*(g+k+tau)
    M[:N,N:2*N] = np.identity(N)*a
    M[N:2*N,:N] = np.identity(N)*theta
    M[N:2*N,N:2*N] = F-np.identity(N)*(k+a+tau)
    M[2*N:3*N,:N] = np.identity(N)*(-theta)
    M[2*N:3*N,2*N:3*N] = F+np.identity(N)*(k-tau)

    A = scipy.linalg.expm(M*T)
    A = A[N:2*N,:] #Subtract the I component

    l,v = np.linalg.eig(np.dot(A.T,A))
    x0 = v[:,np.argmax(l)] #Eigenvector corresponding to the maximum eigenvalue

    #When the length of b is maximized, the length is the maximum eigenvalue
    return ("max_growth = ",np.abs(max(l))/np.linalg.norm(x0)**2,
    "initial condition = ", x0)



def growth3(G,params=(2,2.8,1,1.0,0.5),T=6):
    """
    Question 2.3
    Find maximum possible growth, G=sum(Si Vi)/e(t=0)
    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    T: time for energy growth

    Output:
    G: Maximum growth

    Discussion: Add discussion here
    """

    """
    Likewise, I used the same implementation method for this question. The only
    different part is that I extracted the first N rows and the last N rows,
    and I defined them as B and C. Hence, B and C are the N*3N matrices that
    satisfies the following:
    y(t)=B*x(0) and z(t)=C*x(0), where y(t) consists only of S_i's and z(t)
    only consists of V_i's. x(0) is the same 3N*1 array as last time.

    The question will then lead to finding the maximum of the dot product of
    y(t) and z(t). The dot product can be expressed as the following:
    y(t)^T*z(t) = x(0)^T*(B^T*C)*x(0)

    The format is similar to the previous question besides that B^T*C is not
    symmetric. Hence, if we define it as M, we express M as below:
    1/2(M+M^T)+1/2(M-M^T)

    However, once we put the second term in the equation it will always be 0.
    Hence, we only consider the first term and the process from that is identical
    to the previous procedure, as M+M^T is symmetric.

    Once we compute the maximum eigenvalue and the corresponding eigenvector, we
    obtain the maximum growth of sum(S_i(t=T)*V_i(t=T))/e(t=0) as 68.
    """

    a,theta,g,k,tau=params
    N = G.number_of_nodes()

    #Construct flux matrix (use/modify as needed)
    Q = [t[1] for t in G.degree()]

    Pden = np.zeros(N)
    Pden_total = np.zeros(N)
    for j in G.nodes():
        for m in G.adj[j].keys():
            Pden[j] += Q[m]
    Pden = 1/Pden
    Q = tau*np.array(Q)
    F = nx.adjacency_matrix(G).toarray()
    F = F*np.outer(Q,Pden)
    #-------------------------------------
    G=0
    y = np.zeros(3*N)
    #Add code here

    #Define M
    M  = np.zeros((3*N,3*N))
    M[:N,:N] = F-np.identity(N)*(g+k+tau)
    M[:N,N:2*N] = np.identity(N)*a
    M[N:2*N,:N] = np.identity(N)*theta
    M[N:2*N,N:2*N] = F-np.identity(N)*(k+a+tau)
    M[2*N:3*N,:N] = np.identity(N)*(-theta)
    M[2*N:3*N,2*N:3*N] = F+np.identity(N)*(k-tau)

    A = scipy.linalg.expm(M*T)
    B = A[:N,:] #Extract the S component
    C = A[2*N:3*N,:] #Extract the V component
    D = np.dot(B.T,C) #To compute SV

    l,v = np.linalg.eig(0.5*(D+D.T))
    y0 = v[:,np.argmax(l)] #Eigenvector corresponding to the maximum eigenvalue

    l2,v2 = np.linalg.eig(np.dot(A.T,A))
    x0 = v2[:,np.argmax(l2)] #Eigenvector corresponding to the maximum eigenvalue

    return ("max_growth = ",np.abs(max(l))/np.linalg.norm(x0)**2,
    "initial condition = ",x0)



def Inew(D):
    """
    Question 2.4

    Input:
    D: N x M array, each column contains I for an N-node network

    Output:
    I: N-element array, approximation to D containing "large-variance"
    behavior

    Discussion: Add discussion here
    """

    """
    I implemented the PCA algorithm on D. As a result, my matrix G will identify
    the most important nodes. To be more specific, GG^T implies that the first
    three nodes are the most deterministic ones.

    As the question asks for an n*1 array that will best approximate the total
    variance of I_i, I extracted only one column of G. The column I extracted
    is the one that has the highest corresponding eigenvalue, as the eigenvalue
    corresponds to the "importance" for each column. As the first eigenvalue
    outputs the highest, I outputted the first column of G as my final answer.
    """

    N,M = D.shape
    I = np.zeros(N)

    #Add code here
    #Scale so that the mean of each row is 0
    D2 = D - np.outer(D.mean(axis=1),np.ones((M,1)))

    #Compute SVD
    U,S,VT = np.linalg.svd(D2)
    G = np.dot(U.T,D2)

    #Column that corresponds to the maximum eigenvalue
    return "I_i=",G[:,np.argmax(np.diag(np.dot(G,G.T)))]



if __name__=='__main__':
    G=None
    #add/modify code here if/as desired
    N,M = 100,5
    G = nx.barabasi_albert_graph(N,M,seed=1)

    output1 = growth1(G)
    output2 = growth2(G)
    output3 = growth3(G)

    D = np.loadtxt('q22test.txt')
    output4 = Inew(D)

    print("output1=",output1)
    print("output2=",output2)
    print("output3=",output3)
    print("output4=",output4)
