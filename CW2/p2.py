"""M345SC Homework 2, part 2
Your name and CID here
Sangwoo Jo CID : 01207818
"""
import numpy as np
import networkx as nx
from scipy.integrate import odeint
import matplotlib.pyplot as plt


def model1(G,x=0,params=(50,80,105,71,1,0),tf=6,Nt=400,display=False):
    """
    Question 2.1
    Simulate model with tau=0

    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    tf,Nt: Solutions Nt time steps from t=0 to t=tf (see code below)
    display: A plot of S(t) for the infected node is generated when true

    x: node which is initially infected

    Output:
    S: Array containing S(t) for infected node
    """
    a,theta0,theta1,g,k,tau=params
    tarray = np.linspace(0,tf,Nt+1)

    #Add code here
    #Create a function called RHS2
    def RHS2(siv,t,par):
        theta = theta0+theta1*(1-np.sin(2*np.pi*t))
        return (a*siv[1]-(g+k)*siv[0],
        theta*siv[0]*siv[2]-(k+a)*siv[1],k*(1-siv[2])-theta*siv[0]*siv[2])

    siv0 = (0.05,0.05,0.1) # Initial Parameter

    #Solve the IVP
    siv = odeint(RHS2,siv0,tarray,args=(params,))

    if display == True:
        plt.figure()
        plt.plot(tarray,siv[:,0])
        plt.xlabel('t')
        plt.ylabel('S(t)')
        plt.title("S for the initial infected node vs Time \n Sangwoo Jo p2.model1(G,display=True)")
    return siv[:,0]

def modelN(G,x=0,params=(50,80,105,71,1,0.01),tf=6,Nt=400,display=False):
    """
    Question 2.2
    Simulate model with tau=0

    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    tf,Nt: Solutions Nt time steps from t=0 to t=tf (see code below)
    display: A plot of S(t) for the infected node is generated when true

    x: node which is initially infected

    Output:
    Smean,Svar: Array containing mean and variance of S across network nodes at
                each time step.
    """
    a,theta0,theta1,g,k,tau=params
    tarray = np.linspace(0,tf,Nt+1)

    N = len(G.nodes) # Number of nodes
    S = np.zeros((N,Nt+1)) #Store for the S values for all N nodes
    A = nx.adjacency_matrix(G) #Adjacency matrix of G
    A = A.toarray()

    #An array that stores the degree for each node
    q_dic = G.degree
    q = []
    for i in range(len(q_dic)):
        q.append(q_dic[i])

    #Add code here
    #Design a flux matrix
    sum = np.dot(q,A) #Compute the bottom of the fraction of equation Fij
    F = tau*np.divide(np.multiply(np.reshape(q,(N,1)),A),sum)
    #When sum[j] = 0, redefine Fij = 0
    for j in range(N):
        if sum[j] == 0:
            F[:,j] = 0

    def RHS(y,t):
        """Compute RHS of model at time t
        input: y should be a 3N x 1 array containing with
        y[:N],y[N:2*N],y[2*N:3*N] corresponding to
        S on nodes 0 to N-1, I on nodes 0 to N-1, and
        V on nodes 0 to N-1, respectively.
        output: dy: also a 3N x 1 array corresponding to dy/dt

        Discussion: add discussion here
        """

        """To compute dS/dt for all nodes j = 0,1,...,N-1, I first designed the
        adjacency matrix, degree array and the according flux matrix F above
        the RHS function. The cost of designing each of the array/matrix is 1,N,
        and 2*N maximum, where N is the number of nodes. Hence, the total cost
        so far is 3*N + 1.

        Once the flux matrix F is computed, the RHS has to be designed. First,
        I defined the dy, S, I, V, and the theta array as shown below. Then, I
        computed S1,S2,V1,V2,I1,I2 which are the partial sums from the RHS
        equation. The cost of computing each array is N^2 for the element wise
        multiplication and N for summing them up for each column/row. Finally,
        we can compute dy by running additional 3 assignments.

        Hence, the total running time will be 6*N^2 + 9*N + 9, where N is the
        number of nodes. If we assume that all of the array/matrix I mentioned
        in the first paragraph are already computed, then the total running
        time will be 6*N^2 + 6*N + 8 instead.
        """

        dy = np.zeros((3,N)) #modify

        S = [y[i] for i in range(N)] #Define a separate array that contains S values
        I = [y[i+N] for i in range(N)] #Define a separate array that contains I values
        V = [y[i+2*N] for i in range(N)] #Define a separate array that contains V values

        theta = theta0+theta1*(1-np.sin(2*np.pi*t))

        S1 = (np.multiply(F,S)).sum(axis=1)
        S2 = (np.multiply(F,S)).sum(axis=0)

        I1 = (np.multiply(F,I)).sum(axis=1)
        I2 = (np.multiply(F,I)).sum(axis=0)

        V1 = (np.multiply(F,V)).sum(axis=1)
        V2 = (np.multiply(F,V)).sum(axis=0)


        dy[0,:] = np.multiply(a,I)-np.multiply(g+k,S) +  S1 - S2
        dy[1,:] = np.multiply(theta,np.multiply(S,V))-np.multiply(k+a,I) + I1 - I2
        dy[2,:] = np.multiply(k,1+np.multiply(-1,V))-np.multiply(theta,np.multiply(S,V)) + V1 - V2


        return dy.ravel() # Convert to 3N*1 array

    #Design the input parameter y0
    y0 = np.zeros(3*N)

    y0[x],y0[x+N],y0[x+2*N] = 0.05,0.05,0.1

    if x == 0:
        y0[1:N] = 0
        y0[N+1:2*N] = 0
        y0[2*N+1:3*N] = 1
    elif x == N-1:
        y0[0:N-1] = 0
        y0[N:2*N-1] = 0
        y0[2*N:3*N-1] = 1
    else:
        y0[0:x] = 0
        y0[x+1:N] = 0
        y0[N:N+x] = 0
        y0[N+x+1:2*N] = 0
        y0[2*N:2*N+x] = 1
        y0[2*N+x+1:3*N] = 1

    #Solve the IVP
    S_ivp = odeint(RHS,y0,tarray)
    Smean = S_ivp[:,0:N].mean(axis=1)
    Svar = S_ivp[:,0:N].std(axis=1)

    if display == True:
        plt.figure()
        plt.plot(tarray,Smean)
        plt.plot(tarray,Svar)
        plt.xlabel('t')
        plt.ylabel('Smean(t) & Svar(t)')
        plt.title("Mean & Variance of S vs Time \n Sangwoo Jo p2.modelN(G,display=True)")
        plt.legend(("Mean","Variance"))

    return Smean,Svar



def diffusion(x=0,tf=6,Nt=400,display=False):
    """Analyze similarities and differences
    between simplified infection model and linear diffusion on
    Barabasi-Albert networks.
    Modify input and output as needed.

    Discussion: add discussion here
    """

    """
    Similarities:
    We can observe the similarity once the diffusion constant, D, reaches a certain
    threshold. If we observe fig2, once we observe when the diffusion constant goes
    below 0.0001, both models have a similarity in that the fraction of the
    spreader nodes, S, rarely changes over time.

    To be specific, as shown in fig2, the mean S values for all nodes remain the same.
    around 0.0005. Hence, from both models, we can observe how stable the mean of the
    S values are.

    Differences:
    The difference is how the variance of the S values change over time. As shown in
    fig3, the variance for the simplified infection model gradually decreases over time.
    On the other hand, as shown in fig1, the variance of the S values remains or increases.
    It especially increases dramatically when the diffusion constant is around 0.01.


    """

    tau = 0.01
    #Simplified infection model
    G = nx.random_graphs.erdos_renyi_graph(100,0.3)


    #Linear diffusion on Barabasi-Albert network
    tarray = np.linspace(0,tf,Nt+1)
    Sfin_mean = np.zeros((Nt+1,5))
    Sfin_std = np.zeros((Nt+1,5))

    BA = nx.barabasi_albert_graph(100,5)
    deg = nx.degree(BA)
    N = len(BA.nodes) #Number of nodes

    Q=[]
    #Make a diagonal matrix Q, where the (k,k) element is the degree qk
    for i in range(N):
        Q.append(deg[i])
    #Adjacency matrix of BA
    A = nx.adjacency_matrix(BA)
    A = A.toarray()

    L = Q-A #Laplacian Matrix

    y0 = np.zeros(N)
    y0[x] = 0.05
    #tau+np.zeros(N)

    Smean, Svar = modelN(BA,params=(0,80,0,0,0,0.01))

    #Value of the Diffusion Constant
    d = [0.01,0.005,0.001,0.0001,0.00001]
    #Linear Diffusion Process
    for i in range(5):
        def RHS3(y):
            dy = -d[i]*(np.multiply(L,y).sum(axis=1))
            return dy

        #Solve the IVP
        def IVP(d):
            Sfin = np.zeros((Nt+1,N))
            Sfin[0,:] = y0
            dt = tf/Nt
            for j in range(Nt):
                Sfin[j+1,:]=Sfin[j,:]+RHS3(Sfin[j,:])*dt
            return Sfin

        Sfin_mean[:,i] = IVP(d[i]).mean(axis=1)
        Sfin_std[:,i] = IVP(d[i]).std(axis=1)

    if display == True:
        plt.plot(tarray,Svar,'r--')
        plt.plot(tarray,Sfin_std[:,0], label = '%f' %(d[0]))
        plt.plot(tarray,Sfin_std[:,1], label = '%f' %(d[1]))
        plt.plot(tarray,Sfin_std[:,2], label = '%f' %(d[2]))
        plt.plot(tarray,Sfin_std[:,3], label = '%f' %(d[3]))
        plt.plot(tarray,Sfin_std[:,4], label = '%f' %(d[4]))
        plt.xlabel('t')
        plt.ylabel('S(t)')
        plt.title("Linear Diffusion(Std) vs Time \n Sangwoo Jo p2.diffusion(display=True)")
        plt.legend()

        plt.figure()
        plt.plot(tarray,Smean,'r--')
        plt.plot(tarray,Sfin_mean[:,0], label = '%f' %(d[0]))
        plt.plot(tarray,Sfin_mean[:,1], label = '%f' %(d[1]))
        plt.plot(tarray,Sfin_mean[:,2], label = '%f' %(d[2]))
        plt.plot(tarray,Sfin_mean[:,3], label = '%f' %(d[3]))
        plt.plot(tarray,Sfin_mean[:,4], label = '%f' %(d[4]))
        plt.xlabel('t')
        plt.ylabel('S(t)')
        plt.title("Linear Diffusion(Mean) vs Time \n Sangwoo Jo p2.diffusion(display=True)")
        plt.legend()

    #return modelN(G,params=(0,80,0,0,0,0.01),display=True)
    #return modelN(G,params=(0,80,0,0,0,0.01))[0],Sfin_mean, Sfin_std#modify as needed


if __name__=='__main__':
    #add code here to call diffusion and generate figures equivalent
    #to those you are submitting
    G = nx.random_graphs.erdos_renyi_graph(100,0.3)
    p2.diffusion(display=True)
