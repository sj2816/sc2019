"""M345SC Homework 2, part 1
Your name and CID here
Sangwoo Jo CID : 01207818
"""

import numpy as np

def dfs(L,s):
    """Use the Depth-first search method

    Input:
    L: The dependency array discussed in Part1-1
    s: The source

    Output:
    L3: Compute the shortest distance from the source (Negative direction)
    plus: The furthest distance from the source among the nodes
    vfinal: The node that has the furthest distance from the source
    """
    L1 = len(L)
    L2 = [0 for l in range(L1)] #0 for unexplored, 1 for explored
    L3 = [0 for l in range(L1)] #Shortest distance from the source s (Negative sign)
    #So that the furthest node from the source has a value of 0

    Q=[]
    Q.append(s)
    L2[s]=1
    L3[s]=0
    vfinal= 0
    while len(Q)>0:
        x = Q.pop()
        if L[x] == []:
            continue
        else:
            v = L[x][0]
            vfinal = v
        if L2[v]==0: #If unexplored
            Q.append(v)
            L2[v]=1
            L3[v]=L3[x]-1

    plus = abs(L3[vfinal])
    #Furthest node from the source has a value of 0
    for i in range(L1):
        if L3[i]!=0:
            L3[i]=L3[i]+plus

    L3[s] = L3[s]+plus

    return L3,plus,vfinal

def dfs2(L,s):
    """Use the Depth-first search method

    Input:
    L: The dependency array discussed in Part1-1
    s: The source

    Output:
    L4: An array that will output the nodes that can be reached from the source
    """
    L1 = len(L)
    L2 = [0 for l in range(L1)] #0 for unexplored, 1 for explored
    L4 = []

    Q=[]
    Q.append(s)
    L2[s]=1
    L4.append(s)

    while len(Q)>0:
        x = Q.pop()
        if L[x] == []:
            continue
        else:
            for i in range(len(L[x])): #Loop over L[x]
                v = L[x][i]

                if L2[v]==0: #If unexplored
                    Q.append(v)
                    L4.append(v)
                    L2[v]=1
    return L4

def scheduler(L):
    """
    Question 1.1
    Schedule tasks using dependency list provided as input

    Input:
    L: Dependency list for tasks. L contains N sub-lists, and L[i] is a sub-list
    containing integers (the sub-list my also be empty). An integer, j, in this
    sub-list indicates that task j must be completed before task i can be started.

    Output:
    L: A list of integers corresponding to the schedule of tasks. L[i] indicates
    the day on which task i should be carried out. Days are numbered starting
    from 0.

    Discussion: Add analysis here
    """

    """
    I first designed a schedule plan for the nodes that are dependent to certain
    other nodes. I will first define an initial schedule and update it for every other
    path I can make from a different node. Once the plan is constructed for the
    dependent ones, I constructed a separate schedule plan for the independent
    nodes that will continue from the previously defined schedule.

    There are five definitions I made which cost O(1) for each of them.

    "E" will be the array that will store whether each node has been searched
    or not. "S_dep" array is the array from dfs(L,i)[0] where the element of the
    source will be 0 at first, and rest of the element will be the distance from
    that source. The variable "no" is to update every time where the element of
    the new source will be, since the schedule has to carry on from the previous
    "S_dep" schedule.

    The cost of this for loop will be approximately O(len(L)^2).
    """

    S = np.zeros(len(L)) #Modify as needed
    S_dep = np.zeros(len(L)) #For the dependent tasks

    no = 0
    source = 0
    E = [0 for l in range(len(L))]

    #Define the initial source
    for i in range(len(L)):
        if L[i]!=[]:
            source = dfs(L,i)[2]
            break

    #Sort out the schedule for the dependent tasks
    for i in range(len(L)):
        if L[i]!=[]:
            S_dep = dfs(L,i)[0]
            if E[i] == 0:
                for j in range(len(S)):
                    if S_dep[j]!=0:
                        E[j] = 1 #Marked as explored
                        S_dep[j]=S_dep[j]+no
                dfs_i = dfs(L,i)[2]
                S_dep[dfs_i]=S_dep[dfs_i]+no
                no = no + dfs(L,i)[1]+1
                S=np.add(S,S_dep)

    #Sort out the schedule for the non-dependent tasks
    for i in range(len(L)):
        if S[i]==0:
            if i != source:
                S[i]=S[i]+no
                no = no+1
    return S


def findPath(A,a0,amin,J1,J2):
    """
    Question 1.2 i)
    Search for feasible path for successful propagation of signal
    from node J1 to J2

    Input:
    A: Adjacency list for graph. A[i] is a sub-list containing two-element tuples (the
    sub-list my also be empty) of the form (j,Lij). The integer, j, indicates that there is a link
    between nodes i and j and Lij is the loss parameter for the link.

    a0: Initial amplitude of signal at node J1

    amin: If a>=amin when the signal reaches a junction, it is boosted to a0.
    Otherwise, the signal is discarded and has not successfully
    reached the junction.

    J1: Signal starts at node J1 with amplitude, a0
    J2: Function should determine if the signal can successfully reach node J2 from node J1

    Output:
    L: A list of integers corresponding to a feasible path from J1 to J2.

    Discussion: Add analysis here
    """

    """
    First, given the array A, I extracted the "node" part from each of the element
    in A. I defined it as A0. The cost of defining A0 is approximately
    O(len(A)*(maximum length that A[i] can contain)).

    If J2 is not in J1 or if A[J1] is empty, I returned "None". Otherwise, for
    every node I pass by from J1 I will compute the "Lij" and compare
    a0*Lij and amin with an update. Once the path reaches J2, then the loop
    breaks and output the feasible path existing from J1 to J2.

    """

    L=[] #Modify as needed
    A0 = []#First element for each of the sublist in A

    for i,j in enumerate(A):
        #print(j)
        if type(j[0])==int:
            A0.append([j[0]])
        else:
            B0 = []
            #print(j)
            for k,l in enumerate(j):
                #print(l)
                B0.append(l[0])
            A0.append(B0)

    dfs2_J1 = dfs2(A0,J1)

    #If no tuples exist from node J1
    if A[J1]==[]:
        L.append(None)
    else:
        if J2 not in dfs2_J1: #If J2 not in the adjacency sequence starting from J1
            L.append(None)
        else:
            for i,j in enumerate(dfs2_J1):
                if J2!=j:
                    if type(A[j][0]) == int: #Single list
                        Lij = A[j][1]
                    else:
                        for k,l in enumerate(A[j]): #Multiple list
                            if l[0] == j+1:
                                Lij = l[1]
                    #print(j)
                    if a0*Lij >= amin:
                        print(j)
                        L.append(j)
                    else:
                        L=[]
                        L.append(None)
                        break
                else:
                    break
    if L == [None]:
        L = [J1]+L
    L.append(J2)

    return L


def a0min(A,amin,J1,J2):
    """
    Question 1.2 ii)
    Find minimum initial amplitude needed for signal to be able to
    successfully propagate from node J1 to J2 in network (defined by adjacency list, A)

    Input:
    A: Adjacency list for graph. A[i] is a sub-list containing two-element tuples (the
    sub-list my also be empty) of the form (j,Lij). The integer, j, indicates that there is a link
    between nodes i and j and Lij is the loss parameter for the link.

    amin: Threshold for signal boost
    If a>=amin when the signal reaches a junction, it is boosted to a0.
    Otherwise, the signal is discarded and has not successfully
    reached the junction.

    J1: Signal starts at node J1 with amplitude, a0
    J2: Function should determine min(a0) needed so the signal can successfully
    reach node J2 from node J1

    Output:
    (a0min,L) a two element tuple containing:
    a0min: minimum initial amplitude needed for signal to successfully reach J2 from J1
    L: A list of integers corresponding to a feasible path from J1 to J2 with
    a0=a0min
    If no feasible path exists for any a0, return output as shown below.

    Discussion: Add analysis here
    """

    """The code is nearly identical apart from the part where I define a0. As shown
    in bottom of the code, I redefine a0 as amin/Lij for every update required.
    Hence, the final a0 value will be the minimum a0 value that will let any
    signal from J1 to J2 to successfully propogate through.
    """

    L=[] #Modify as needed
    A0 = []#First element for each of the sublist in A
    a0 = 0 #Initial amplitude parameter

    for i,j in enumerate(A):
        #print(j)
        if type(j[0])==int:
            A0.append([j[0]])
        else:
            B0 = []
            #print(j)
            for k,l in enumerate(j):
                #print(l)
                B0.append(l[0])
            A0.append(B0)

    print(A0)
    #print(A0,dfs2(A0,J1))
    #If no tuples exist from node J1
    if A[J1]==[]:
        L.append(None)
    else:
        if J2 not in dfs2(A0,J1): #If J2 not in the adjacency sequence starting from J1
            L.append(None)
        else:
            for i,j in enumerate(dfs2(A0,J1)):
                if J2!=j:
                    Lij = 0
                    if type(A[j][0]) == int: #Single list
                        Lij = A[j][1]
                    else:
                        for k,l in enumerate(A[j]): #Multiple list
                            if l[0] == j+1:
                                Lij = l[1]
                    #print(j)
                    if a0*Lij >= amin:
                        print(j)
                        L.append(j)
                    else:
                        a0 = amin/Lij
                        L.append(j)
                else:
                    break

    if L == [None]:
        output = -1,[] #Modify as needed
    else:
        L.append(J2)
        output = a0,L
    return output



if __name__=='__main__':
    #add code here if/as desired
    L= [[2],[4],[3],[],[5],[6],[]] #modify as needed
    p1.scheduler(L)
